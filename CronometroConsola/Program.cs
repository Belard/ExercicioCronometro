﻿

namespace CronometroConsola
{
    using System;
    using ExercicioCronometro;
    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();
            Console.WriteLine("Pressione Enter para iniciar o cronómetro");
            Console.ReadLine();

            relogio.StartClock();
            Console.WriteLine("Pressione Enter para parar o cronómetro");

            while (relogio.ClockState())
            {
                var tempo = DateTime.Now - relogio.StartTime();
                Console.Write("\r Tempo corrente: {0}", tempo);

                if (Console.KeyAvailable)
                {
                    if(Console.ReadKey().Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            }
            relogio.StopClock();

            Console.WriteLine("\r Tempo cronometrado: {0}", relogio.GetTimeSpan());
            Console.ReadLine();
        }
    }
}
